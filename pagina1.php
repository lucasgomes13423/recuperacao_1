<?php
$usuario = $_POST["usuario"];
$email = $_POST["email"];
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <link rel="stylesheet" href="estilos.css">
  <title>Recuperação 1</title>

</head>

<body>
  <h1>Recuperação 1 - HTML, CSS, Javascript, PHP</h1>
  <hr>
  <b>
    <p>Aluno: Lucas Gabriel Gomes Neto - Turma: Aut2D1</p>
  </b>
  <hr>
  <b>
    <p>Dados Recebidos:</p>
  </b>
  <hr>
  <div class='resposta'>
    <p>O usuário é: <?php echo $usuario; ?></p>
  </div>
  <div class='resposta'>
    <p>O email é: <?php echo $email; ?></p>
  </div>
  <a href="./index.php">
    <p>Voltar para a página inicial</p>
  </a>

</body>

</html>