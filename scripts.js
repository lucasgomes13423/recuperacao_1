const botao = window.document.querySelector('button')

botao.onclick = function (event) {
  const usuario = window.document.getElementById('usuario')
  const email = window.document.getElementById('email')

  if (usuario.value === '' || email.value === '') {
    alert('Preencha todos os campos!')
    event.preventDefault()
  }
}